#!/bin/bash
# git svn clone --stdlayout --authors-file=authors.txt --no-metadata  http://www.its.net.gr:81/svn/ITS.Workflow.Platform ITS.Workflow.Platform
# cp filename.sh sales/
cd sales/
# cp -Rf .git/refs/remotes/origin/tags/* .git/refs/tags/
# rm -Rf .git/refs/remotes/origin/tags
# cp -Rf .git/refs/remotes/* .git/refs/heads/
# rm -Rf .git/refs/remotes
# ./filename.sh
# rm -rf ./filename.sh
# git tag | xargs git tag -d
# git filter-branch --tag-name-filter cat --index-filter 'git rm -r --cached --ignore-unmatch filename' --prune-empty -f -- --all
# rm -rf .git/refs/original/
# git reflog expire --expire=now --all
# git gc --prune=now
# git gc --aggressive --prune=now
# git remote add origin http://loki:8080/tfs/ITS/_git/sales
# git push -u -f origin --all
# git push -f --tag
# git config --add svn-remote.2.9.mobilespeedup.url http://www.its.net.gr:81/svn/sales/branches/2.9.mobilespeedup
# git config --add svn-remote.2.9.mobilespeedup.fetch :refs/remotes/2.9.mobilespeedup
# git svn fetch 2.9.mobilespeedup
# git checkout -b 2.9.mobilespeedup 2.9.mobilespeedup
# git svn rebase 2.9.mobilespeedup
# git push -u -f origin --all
git config --add svn-remote.v2.6.1.url http://www.its.net.gr:81/svn/sales/branches/v2.6.1
git config --add svn-remote.v2.6.1.fetch :refs/remotes/v2.6.1
git svn fetch v2.6.1
git checkout -b v2.6.1 v2.6.1
git svn rebase v2.6.1
git push -u -f origin --all