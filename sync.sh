#!/bin/bash
git svn clone --stdlayout --authors-file=authors.txt --no-metadata  http://www.its.net.gr:81/svn/$1
cp filename.sh $1/
cd $1/
cp -Rf .git/refs/remotes/origin/tags/* .git/refs/tags/
rm -Rf .git/refs/remotes/origin/tags
cp -Rf .git/refs/remotes/* .git/refs/heads/
rm -Rf .git/refs/remotes
./filename.sh
rm -rf ./filename.sh
git tag | xargs git tag -d
git filter-branch --tag-name-filter cat --index-filter 'git rm -r --cached --ignore-unmatch filename' --prune-empty -f -- --all
rm -rf .git/refs/original/
git reflog expire --expire=now --all
git gc --prune=now
git gc --aggressive --prune=now
git remote add origin http://loki:8080/tfs/ITS/_git/$1
git push -u -f origin --all
git push -f --tag